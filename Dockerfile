FROM beh01der/logstash-es-kibana

EXPOSE 5001
COPY conf/logstash.conf /opt/logstash/conf
