### Configuracion docker-compose
Esta configuracion depende de _volumes_ y _networks_ externos. Estos se han de crear previamente. En el caso de __network__ se esta usando la misma que usa la configuracion __jenkins__ lo que permite la conectividad entre los dos contenedores.
La configuracion de jenkins requiere el binario hpi __(logstash.hpi)__ que corresponde al plugin de logstash. Se puede descargar desde [aqui](http://updates.jenkins-ci.org/latest/logstash.hpi). 
Se comparte adicionalmente la configuracion logstash de la carpeta _conf_ (sustituir con la ruta absoluta local apropiada)
Circunstancialmente se incorpora un contenedor de gestión gráfica docker llamando [portainer](https://github.com/portainer/portainer)


[docker-compose.yml](./docker-compose.yml)
```yaml
version: '2'

services:
  logstash-kibana:
    build: .
    image: logstash-kibana-ext
    container_name: logstash
    ports:
      - "5601:5601"
      - "5001:5001"
    networks:
      jenkins2_net:
        aliases:
          - logstash
      
    volumes:
     
      - /development/repos/logstash-jenkins-docker/conf:/opt/logstash/conf
      - logstash_data:/opt/elasticsearch/data
      - logstash_data:/opt/kibana/conf

  jenkins:
    image: jenkins:2.19.1
    container_name: jenkins_for_logstash
    networks:
      jenkins2_net:
        aliases:
          - jenkins_docker
    environment:
      JAVA_OPTS: "-Djava.awt.headless=true"
    ports:
      - "50000:50000"
      - "8080:8080"
    volumes:
      - jenkins_data:/var/jenkins_home      
      - /development/repos/logstash-jenkins-docker/hpi:/var/jenkins_home/plugins

  portainer:
    image: portainer/portainer:latest
    container_name: portainer
    ports: 
      - "9000:9000"
    networks:
      jenkins2_net:
        aliases:
          - portainer
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

volumes:
  logstash_data:
    external: true
  jenkins_data:
    external: true
  portainer_data:
    external: true

networks:
  jenkins2_net:
    external: true
```
## logstash-kibana-ext
La imagen _logstash-kibana-ext_ es una extension de la imagen [beh01der/logstash-es-kibana](https://hub.docker.com/r/beh01der/logstash-es-kibana/)

## logstash.conf
[logstash.conf](conf/logstash.conf)
Se incorpora un script ruby para tratar de forma dinamica un conjunto de variables. La cadena a tratar tiene el siguiente formato:
```
\"buildVariables\":{\"bar\":\"Bar Diamante\"}
```


```ruby
input { 
  file {
    path => "/opt/logstash/example/access.log"  
    start_position => "beginning"
    type => file

  }
  syslog {
    port => 5001
    type => syslog
  }

} 

filter {


  if [type] == "syslog" {
    grok {
      patterns_dir => ["/opt/logstash/conf/patterns"]
      match => { "message" => "%{JENKINS_BUILD}"}
      add_field => [ "received_at", "%{@timestamp}" ]
      add_field => [ "received_from", "%{host}" ]

      
    }

    grok {
      patterns_dir => ["/opt/logstash/conf/patterns"]
      match => { "message" => "%{JENKINS_BUILD_VARIABLES},"}
    }
    
    
    ruby {
        code => "
            
            fieldArray = event['build_variables'].split(',')
            for field in fieldArray
                field = field.delete '\"'
                
                result = field.split(':')
                event[result[0]] = result[1]
            end
        "
    }

    date {
      match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
    }



  }

  if [type] == "file" {

    grok {
      match => ["message", "%{COMMONAPACHELOG}"]
    }

    date { 
      match => [ "timestamp", "dd/MMM/yyyy:HH:mm:ss Z" ]
    }
  }
}


output { 
  if [type] == "syslog" {
    elasticsearch { 
      host => localhost
      index => "jenkins-%{+YYYY.MM.dd}"
    } 
  }
  if [type] == "file" {
    elasticsearch { 
      host => localhost 
      
    } 
  }
  stdout {
    codec => rubydebug
  }
}

```